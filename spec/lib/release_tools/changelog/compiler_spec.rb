# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Changelog::Compiler do
  let(:project) { 'gitlab-org/gitlab' }
  let(:client) { class_spy(ReleaseTools::GitlabClient) }

  describe '#compile' do
    it 'compiles the changelog' do
      compiler = described_class.new(project, client: client)
      version = ReleaseTools::Version.new('10.0.0')
      entries = [
        ReleaseTools::Changelog::Entry.new(
          'changelogs/unreleased/foo.yml',
          YAML.dump('title' => 'test', 'type' => 'feature')
        )
      ]

      allow(compiler)
        .to receive(:unreleased_changelog_entry_paths)
        .with(version.stable_branch)
        .and_return(entries.map(&:path))

      allow(compiler)
        .to receive(:unreleased_changelog_entry_paths)
        .with('master')
        .and_return(entries.map(&:path))

      allow(compiler)
        .to receive(:unreleased_changelog_entries)
        .with(entries.map(&:path), version.stable_branch)
        .and_return(entries)

      allow(compiler)
        .to receive(:compile_entries)
        .with(version, entries, entries.map(&:path), version.stable_branch)

      allow(compiler)
        .to receive(:compile_entries)
        .with(version, entries, entries.map(&:path), 'master')

      compiler.compile(version)

      expect(compiler).to have_received(:compile_entries).twice
    end

    it 'compiles the master changelog using paths that exist on master' do
      compiler = described_class.new(project, client: client)
      version = ReleaseTools::Version.new('10.0.0')
      entries = [
        ReleaseTools::Changelog::Entry.new(
          'changelogs/unreleased/foo.yml',
          YAML.dump('title' => 'test', 'type' => 'feature')
        ),
        ReleaseTools::Changelog::Entry.new(
          'changelogs/unreleased/bar.yml',
          YAML.dump('title' => 'bar', 'type' => 'feature')
        )
      ]

      allow(compiler)
        .to receive(:unreleased_changelog_entry_paths)
        .with(version.stable_branch)
        .and_return(entries.map(&:path))

      allow(compiler)
        .to receive(:unreleased_changelog_entry_paths)
        .with('master')
        .and_return([entries.first.path])

      allow(compiler)
        .to receive(:unreleased_changelog_entries)
        .with(entries.map(&:path), version.stable_branch)
        .and_return(entries)

      allow(compiler)
        .to receive(:compile_entries)
        .with(version, entries, entries.map(&:path), version.stable_branch)

      allow(compiler)
        .to receive(:compile_entries)
        .with(version, entries, [entries.first.path], 'master')

      compiler.compile(version)

      expect(compiler).to have_received(:compile_entries).twice
    end

    it 'does not compile on master if this is disabled' do
      compiler = described_class.new(project, client: client)
      version = ReleaseTools::Version.new('10.0.0')
      entries = [
        ReleaseTools::Changelog::Entry.new(
          'changelogs/unreleased/foo.yml',
          YAML.dump('title' => 'test', 'type' => 'feature')
        )
      ]

      allow(compiler)
        .to receive(:unreleased_changelog_entry_paths)
        .with(version.stable_branch)
        .and_return(entries.map(&:path))

      allow(compiler)
        .to receive(:unreleased_changelog_entries)
        .with(entries.map(&:path), version.stable_branch)
        .and_return(entries)

      allow(compiler)
        .to receive(:compile_entries)
        .with(version, entries, entries.map(&:path), version.stable_branch)

      compiler.compile(version, master: false)

      expect(compiler).to have_received(:compile_entries)
    end
  end

  describe '#compile_entries' do
    around do |example|
      # Changelog compilation produces headers that include a date. Here we
      # freeze the time to make sure the same date is produced.
      Timecop.freeze(Time.new(2020, 6, 16)) do
        example.run
      end
    end

    before do
      markdown = <<~MARKDOWN
        ## 8.10.4 (2020-06-16)

        ### Other (1 change)

        - Change A.
      MARKDOWN

      allow(client)
        .to receive(:get_file)
        .with(project, described_class::DEFAULT_CHANGELOG_FILE, 'master')
        .and_return(
          double(
            :file,
            content: Base64.encode64(markdown.strip),
            last_commit_id: '123'
          )
        )
    end

    it 'compiles all changelog entries' do
      compiler = described_class.new(project, client: client)
      version = ReleaseTools::Version.new('10.0.0')
      entries = [
        ReleaseTools::Changelog::Entry.new(
          'changelogs/unreleased/foo.yml',
          YAML.dump('title' => 'test', 'type' => 'feature')
        )
      ]

      allow(compiler)
        .to receive(:commit_changes)
        .with(
          version,
          entries.map(&:path),
          'master',
          a_string_including('test'),
          '123'
        )

      compiler.compile_entries(version, entries, entries.map(&:path), 'master')

      expect(compiler).to have_received(:commit_changes)
    end

    it 'does not update the changelog with already existing content' do
      compiler = described_class.new(project, client: client)
      version = ReleaseTools::Version.new('8.10.4')
      entries = [
        ReleaseTools::Changelog::Entry.new(
          'changelogs/unreleased/foo.yml',
          YAML.dump('title' => 'Change A', 'type' => 'feature')
        )
      ]

      allow(compiler).to receive(:commit_changes)

      compiler.compile_entries(version, entries, entries.map(&:path), 'master')

      expect(compiler).not_to have_received(:commit_changes)
    end

    it 'handles different encodings used for the old and new changelog' do
      compiler = described_class.new(project, client: client)
      version = ReleaseTools::Version.new('8.10.4')
      entries = [
        ReleaseTools::Changelog::Entry.new(
          'changelogs/unreleased/foo.yml',
          YAML.dump('title' => 'Change A 👍', 'type' => 'feature')
        )
      ]

      markdown = <<~MARKDOWN
        ## 8.10.4 (2020-06-16)

        ### Other (1 change)

        - Change A. 👍
      MARKDOWN

      allow(Base64)
        .to receive(:decode64)
        .and_return(markdown.dup.force_encoding(Encoding::ASCII_8BIT))

      allow(compiler).to receive(:commit_changes)

      expect do
        compiler.compile_entries(version, entries, entries.map(&:path), 'master')
      end.not_to raise_error
    end

    it 'does not update an existing section with an empty section' do
      compiler = described_class.new(project, client: client)
      version = ReleaseTools::Version.new('8.10.4')

      allow(compiler).to receive(:commit_changes)

      compiler.compile_entries(version, [], [], 'master')

      expect(compiler).not_to have_received(:commit_changes)
    end

    it 'does create a new section when there are no entries for a new version' do
      compiler = described_class.new(project, client: client)
      version = ReleaseTools::Version.new('8.11.0')

      allow(compiler)
        .to receive(:commit_changes)
        .with(version, [], 'master', a_string_including('8.11.0'), '123')

      compiler.compile_entries(version, [], [], 'master')

      expect(compiler).to have_received(:commit_changes)
    end

    it 'retries compiling the entries when there is an API error' do
      compiler = described_class.new(project, client: client)
      version = ReleaseTools::Version.new('10.0.0')
      entries = [
        ReleaseTools::Changelog::Entry.new(
          'changelogs/unreleased/foo.yml',
          YAML.dump('title' => 'test', 'type' => 'feature')
        )
      ]

      attempt = 0

      allow(compiler).to receive(:commit_changes) do
        next unless attempt.zero?

        attempt += 1
        raise gitlab_error(:NotFound)
      end

      compiler.compile_entries(version, entries, entries.map(&:path), 'master')

      expect(compiler)
        .to have_received(:commit_changes)
        .with(
          version,
          entries.map(&:path),
          'master',
          a_string_including('test'),
          '123'
        )
        .twice
    end
  end

  describe '#commit_changes' do
    it 'creates a commit to update a changelog and remove unreleased entries' do
      compiler = described_class.new(project, client: client)

      allow(client)
        .to receive(:create_commit)
        .with(
          project,
          'foo',
          "Update CHANGELOG.md for 1.2.3\n\n[ci skip]",
          [
            { action: 'delete', file_path: 'changelogs/unreleased/foo.yml' },
            {
              action: 'update',
              file_path: described_class::DEFAULT_CHANGELOG_FILE,
              last_commit_id: '123',
              content: 'bar'
            }
          ]
        )
        .and_return(double(:commit, id: '456'))

      compiler.commit_changes(
        ReleaseTools::Version.new('1.2.3'),
        %w[changelogs/unreleased/foo.yml],
        'foo',
        'bar',
        '123'
      )

      expect(client).to have_received(:create_commit)
    end
  end

  describe '#unreleased_changelog_entry_paths' do
    it 'returns the paths of unreleased changelog entries' do
      entry1 =
        double(:entry, name: 'foo.yml', path: 'changelogs/unreleased/foo.yml')

      entry2 =
        double(:entry, name: '.gitkeep', path: 'changelogs/unreleased/.gitkeep')

      compiler = described_class.new(project, client: client)
      page = Gitlab::PaginatedResponse.new([entry1, entry2])

      allow(client)
        .to receive(:tree)
        .with(
          project,
          ref: 'foo',
          path: described_class::DEFAULT_SOURCE,
          per_page: 100
        )
        .and_return(page)

      paths = compiler.unreleased_changelog_entry_paths('foo')

      expect(paths).to eq([entry1.path])
    end
  end

  describe '#unreleased_changelog_entries' do
    it 'returns an Array of changelog entries' do
      compiler = described_class.new(project, client: client)
      path = 'changelogs/unreleased/foo.yml'

      allow(client)
        .to receive(:file_contents)
        .with(project, path, 'foo')
        .and_return("---\ntitle: foo")

      entries = compiler.unreleased_changelog_entries([path], 'foo')

      expect(entries.length).to eq(1)
      expect(entries.first.path).to eq('changelogs/unreleased/foo.yml')
    end
  end
end
