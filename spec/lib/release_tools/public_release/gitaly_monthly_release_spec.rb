# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::PublicRelease::GitalyMonthlyRelease do
  describe '#initialize' do
    it 'converts EE versions to CE versions' do
      version = ReleaseTools::Version.new('42.0.0-ee')
      release = described_class.new(version)

      expect(release.version).to eq('42.0.0')
    end
  end

  describe '#execute' do
    it 'runs the release' do
      version = ReleaseTools::Version.new('42.0.0')
      release = described_class.new(version)
      tag = double(:tag, name: 'v42.0.0')

      expect(release).to receive(:create_target_branch)
      expect(release).to receive(:compile_changelog)
      expect(release).to receive(:update_versions)

      expect(release).to receive(:create_tag).and_return(tag)

      expect(release)
        .to receive(:add_release_metadata)
        .with(tag)

      expect(release)
        .to receive(:notify_slack)
        .with(ReleaseTools::Project::Gitaly, version)

      release.execute
    end

    it 'does not run the release for an RC version' do
      version = ReleaseTools::Version.new('42.0.0-rc42')
      release = described_class.new(version)

      expect(release).not_to receive(:create_target_branch)
      expect(release).not_to receive(:compile_changelog)
      expect(release).not_to receive(:update_versions)

      release.execute
    end
  end

  describe '#compile_changelog' do
    it 'compiles the changelog' do
      client = class_spy(ReleaseTools::GitlabClient)
      version = ReleaseTools::Version.new('42.0.0')
      release = described_class.new(version, client: client)
      compiler = instance_spy(ReleaseTools::Changelog::Compiler)

      expect(ReleaseTools::Changelog::Compiler)
        .to receive(:new)
        .with(release.project.canonical_or_security_path, client: client)
        .and_return(compiler)

      expect(compiler)
        .to receive(:compile)
        .with(version, branch: '42-0-stable')

      release.compile_changelog
    end
  end

  describe '#source_for_target_branch' do
    it 'returns the version from GITALY_SERVER_VERSION in GitLab EE' do
      client = class_spy(ReleaseTools::GitlabClient)
      version = ReleaseTools::Version.new('42.0.0-ee')
      release = described_class.new(version, client: client)

      allow(client)
        .to receive(:file_contents)
        .with(
          ReleaseTools::Project::GitlabEe.canonical_or_security_path,
          'GITALY_SERVER_VERSION',
          '42-0-stable-ee'
        )
        .and_return("abc\n")

      expect(release.source_for_target_branch).to eq('abc')
    end

    it 'converts version names into tag names' do
      client = class_spy(ReleaseTools::GitlabClient)
      version = ReleaseTools::Version.new('42.0.0-ee')
      release = described_class.new(version, client: client)

      allow(client)
        .to receive(:file_contents)
        .with(
          ReleaseTools::Project::GitlabEe.canonical_or_security_path,
          'GITALY_SERVER_VERSION',
          '42-0-stable-ee'
        )
        .and_return("1.2.3\n")

      expect(release.source_for_target_branch).to eq('v1.2.3')
    end

    it 'retries the operation when the API returns an error' do
      client = class_spy(ReleaseTools::GitlabClient)
      version = ReleaseTools::Version.new('42.0.0-ee')
      release = described_class.new(version, client: client)
      raised = false

      allow(client).to receive(:file_contents) do
        if raised
          'abc'
        else
          raised = true
          raise gitlab_error(:InternalServerError)
        end
      end

      expect(release.source_for_target_branch).to eq('abc')
    end
  end
end
