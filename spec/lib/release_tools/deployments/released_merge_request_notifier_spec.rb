# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Deployments::ReleasedMergeRequestNotifier do
  describe '#notify' do
    let(:deploy) do
      ReleaseTools::Deployments::DeploymentTracker::Deployment
        .new(ReleaseTools::Project::GitlabEe, 1, 'success')
    end

    it 'notifies merge requests when deploying a patch release' do
      updater = instance_spy(ReleaseTools::Deployments::MergeRequestUpdater)

      allow(ReleaseTools::Deployments::MergeRequestUpdater)
        .to receive(:for_successful_deployments)
        .with([deploy])
        .and_return(updater)

      allow(updater)
        .to receive(:add_comment)
        .with(/12.5.2 release.+\/label ~published/m)

      described_class.notify('pre', [deploy], '12.5.2')

      expect(updater).to have_received(:add_comment)
    end

    it 'does not notify merge requests when deploying to staging' do
      expect(ReleaseTools::Deployments::MergeRequestUpdater)
        .not_to receive(:for_successful_deployments)

      described_class.notify('gstg', [deploy], '12.5.0.ee.0')
    end

    it 'does not notify merge requests when deploying to production' do
      expect(ReleaseTools::Deployments::MergeRequestUpdater)
        .not_to receive(:for_successful_deployments)

      described_class.notify('gprd', [deploy], '12.5.0.ee.0')
    end

    it 'does not notify merge requests when deploying to canary' do
      expect(ReleaseTools::Deployments::MergeRequestUpdater)
        .not_to receive(:for_successful_deployments)

      described_class.notify('gprd-cny', [deploy], '12.5.0.ee.0')
    end

    it 'does not notify merge requests when deploying an unsupported version' do
      expect(ReleaseTools::Deployments::MergeRequestUpdater)
        .not_to receive(:for_successful_deployments)

      described_class.notify('pre', [deploy], '12.5.0.4.5.6.ee.0')
    end

    it 'does not notify merge requests when deploying a stable release' do
      expect(ReleaseTools::Deployments::MergeRequestUpdater)
        .not_to receive(:for_successful_deployments)

      described_class.notify('pre', [deploy], '12.5.0')
    end

    it 'does not notify merge requests when deploying an RC' do
      expect(ReleaseTools::Deployments::MergeRequestUpdater)
        .not_to receive(:for_successful_deployments)

      described_class.notify('pre', [deploy], '12.5.0-rc43.ee.0')
    end
  end
end
