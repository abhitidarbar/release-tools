# frozen_string_literal: true

require 'spec_helper'

describe ReleaseTools::Promotion::ProductionStatus do
  subject(:status) { described_class.new }

  describe '#fine?' do
    context 'when all the checks are fine' do
      it 'is fine' do
        checks = [
          double('check 1', fine?: true),
          double('check 2', fine?: true)
        ]

        expect(status).to receive(:checks).and_return(checks)

        expect(status).to be_fine
      end
    end

    context 'when at least one check failed' do
      it 'is not fine' do
        checks = [
          double('check 1', fine?: false),
          double('check 2', fine?: true)
        ]

        expect(status).to receive(:checks).and_return(checks)

        expect(status).not_to be_fine
      end
    end
  end
end
