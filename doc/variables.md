# release-tools CI variables

This project makes heavy use of environment variables for configuration. This
document aims to provide a reference for the most important ones, but is not
necessarily comprehensive.

These are the [CICD variables](https://gitlab.com/gitlab-org/release-tools/settings/ci_cd) that are
defined in the [release-tools project](https://gitlab.com/gitlab-org/release-tools).

## Instance tokens

| Variable Name                  | Deployment         | Token name\*  | Scopes       | User                                                      |
| ------------                   | ------------       | ------------  | ------------ | ------------                                              |
| `RELEASE_BOT_DEV_TOKEN`        | dev.gitlab.org     | release-tools | api          | [@gitlab-release-tools-bot][gitlab-release-tools-bot-dev] |
| `RELEASE_BOT_OPS_TOKEN`        | ops.gitlab.net     | Release token | api          | [@gitlab-release-tools-bot][gitlab-release-tools-bot-ops] |
| `RELEASE_BOT_PRODUCTION_TOKEN` | gitlab.com         | release-tools | api          | [@gitlab-release-tools-bot][gitlab-release-tools-bot-com] |
| `RELEASE_BOT_VERSION_TOKEN`    | version.gitlab.com | private token | api          | robert+release-tools@gitlab.com                           |

_* Token name refers to the name that was entered when the token was created_

## SSH private keys

Private keys are used to push to repositories via SSH, rather than
authenticating over HTTPS with an access token.

- `RELEASE_BOT_PRIVATE_KEY` -- Private key for
  [@gitlab-release-tools-bot][gitlab-release-tools-bot-com].

## Auto-deploy

- `AUTO_DEPLOY_BRANCH` -- The current auto-deploy branch. Gets updated via API
  by auto-deploy jobs and **should not be changed manually.**
- `OMNIBUS_BUILD_TRIGGER_TOKEN` -- Used to trigger an Omnibus build.
- `HELM_BUILD_TRIGGER_TOKEN` -- Used to trigger an Helm charts auto-deploy tagging.
- `SENTRY_AUTH_TOKEN` -- Used to create releases and deploys in Sentry. Requires the `project:releases` API scope.
- `IGNORE_PRODUCTION_CHECKS` -- The reason for bypassing the production checks. If set to `false` checks will not be skipped.

## Feature flags

- `FEATURE_INSTANCE_URL` -- Unleash endpoint for project feature flags
- `FEATURE_INSTANCE_ID` -- Unleash instance ID for project feature flags

## Metrics

- `PROMETHEUS_HOST` -- The `hostname:port` to a Prometheus instance, used for
  gathering current host versions for GitLab environments.
- `PUSHGATEWAY_URL` -- The full URL to a Pushgateway, where metrics gathered by
  this project will be pushed.

See the [metrics documentation](./metrics.md) for more information.

## Miscellany

- `SENTRY_DSN` -- DSN for the `release-tools` project on
  [Sentry](https://sentry.gitlab.net/gitlab/release-tools/).
- `SLACK_CHATOPS_URL` -- Full Slack webhook URL for ChatOps responses.
- `SLACK_TAG_URL` -- Full Slack webhook URL for tagging notifications.
  notifications.
- `SLACK_WRAPPER_URL` -- Endpoint for [`slack-wrapper`](https://ops.gitlab.net/gitlab-com/gl-infra/infra-automation-commons/slack-wrapper)
- `SLACK_WRAPPER_TOKEN` -- `slack-wrapper` access token

[gitlab-release-tools-bot-com]: https://gitlab.com/gitlab-release-tools-bot
[gitlab-release-tools-bot-dev]: https://dev.gitlab.org/gitlab-release-tools-bot
[gitlab-release-tools-bot-ops]: https://ops.gitlab.net/gitlab-release-tools-bot
[deployer-ops]: https://ops.gitlab.net/deployer
[gitlab-bot-com]: https://gitlab.com/gitlab-bot

## AutoDeploy Variables

Many variables are associated with how deployments are completed.  This list can
be found here:
https://gitlab.com/gitlab-org/release/docs/-/blob/master/runbooks/variables.md

---

[Return to Documentation](../README.md#documentation)
