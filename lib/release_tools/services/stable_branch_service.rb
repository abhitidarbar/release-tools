# frozen_string_literal: true

module ReleaseTools
  module Services
    class StableBranchService
      include ::SemanticLogger::Loggable
      include BranchCreation

      GITLAB_COM_GPRD_ENVIRONMENT_ID = 1_178_942

      def initialize(version)
        @version = version
      end

      def gitlab_client
        ReleaseTools::GitlabClient
      end

      # Creates stable branches for GitLab, GitLab FOSS, Omnibus GitLab,
      # CNGImage and Helm projects
      #
      # Stable branches are created based on 'source'
      #
      # => create_stable_branches('13-0-auto-deploy-20200520')
      #
      # If the current auto-deploy branch is used, stable branches for
      # GitLab, Omnibus are created from the last deployment to production.
      # GitLab FOSS stable branch is created based on the past stable branch.
      #
      # => create_stable_branches(6dbf7917e82701341e6d9056392d7b4b98673ef9)
      #
      # If a SHA is used, stable branches are created based on that SHA for GitLab
      #
      # Stable branches for CNG and HELM are always created from master branch.
      #
      # NOTE: Due to the possibility of creating a stable branch from an
      # auto-deploy branch, which may only exist on Security, we also create the
      # stable branches on the same remote.
      def create_stable_branches(source)
        logger.info('Creating stable branches', source: source, version: @version)

        ce_branch = @version.stable_branch(ee: false)
        ee_branch = @version.stable_branch(ee: true)

        if source == current_auto_deploy_branch
          logger.info('Creating stable branch from last deployment production deployment')

          deployment = gitlab_client.last_deployment(Project::GitlabEe, GITLAB_COM_GPRD_ENVIRONMENT_ID)

          logger.info('Last deployment', ref: deployment.ref, sha: deployment.sha, date: deployment.created_at)

          source = deployment.sha

          omnibus_source = deployment.ref
        else
          omnibus_source = current_auto_deploy_branch
        end

        create_branch_from_ref(Project::GitlabEe.auto_deploy_path, ee_branch, source)
        create_branch_from_ref(Project::OmnibusGitlab.auto_deploy_path, ce_branch, omnibus_source)

        # CNG and Charts don't have an auto-deploy dependency, so they get
        # created from master.
        create_branch_from_ref(Project::CNGImage.auto_deploy_path, ce_branch, 'master')

        # Helm charts follow different branching scheme
        create_helm_branch

        # FOSS gets created directly from its previous stable branch
        create_ce_stable_branch(ce_branch)
      end

      # For CE we want to base the stable branch on the previous stable branch.
      # This way we don't end up with commits from "master" that are
      # useless/reverted once we perform a FOSS sync from EE to CE. Including
      # those commits may lead one to believe unrelated changes are in CE, when
      # this is not the case.
      def create_ce_stable_branch(branch)
        source =
          Version.new(@version.previous_minor).stable_branch(ee: false)

        create_branch_from_ref(Project::GitlabCe, branch, source)
      end

      def create_helm_branch
        project = ReleaseTools::Project::HelmGitlab
        repo = ReleaseTools::RemoteRepository.get(project.remotes)

        version_manager = ReleaseTools::Helm::VersionManager.new(repo)
        helm_version = version_manager.next_version(@version.to_ce)

        create_branch_from_ref(project.auto_deploy_path, helm_version.stable_branch, 'master')
      ensure
        repo.cleanup
      end

      private

      def current_auto_deploy_branch
        @current_auto_deploy_branch ||= ReleaseTools::AutoDeployBranch.current_name
      end
    end
  end
end
