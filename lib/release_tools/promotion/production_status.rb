# frozen_string_literal: true

module ReleaseTools
  module Promotion
    class ProductionStatus
      include ::SemanticLogger::Loggable
      include Check

      def checks
        @checks ||= [
          Checks::ActiveIncidents.new,
          Checks::ChangeRequests.new,
          Checks::GitlabDeploymentHealth.new
        ]
      end

      # @see Check#fine?
      def fine?
        checks.all?(&:fine?)
      end

      # @see Check#to_issue_body
      def to_issue_body
        text = StringIO.new
        if fine?
          text.puts("#{ok_icon} Production has no active incidents or in-progress change issues. Per [described process](https://about.gitlab.com/handbook/engineering/releases/#gitlabcom-releases-1), the deployment is automatically authorized.")
        else
          text.puts("#{failure_icon} The deployment cannot progress because there are blockers")
        end

        checks.each do |check|
          text.puts("\n---")
          text.puts(check.to_issue_body)
        end

        text.string
      end

      # @see Check#to_slack_block
      def to_slack_block
        overall_status = if fine?
                           "#{ok_icon} We can start a deployment now! :shipit: :fine:"
                         else
                           "#{failure_icon} A deployment cannot start now!"
                         end
        {
          type: 'section',
          text: mrkdwn(overall_status)
        }
      end

      def to_slack_blocks
        [to_slack_block] + checks.map(&:to_slack_block)
      end
    end
  end
end
