# frozen_string_literal: true

module ReleaseTools
  module Promotion
    # BakingTimeForeword provides an introductory Slack block for the baking time report
    class BakingTimeForeword
      SLACK_CHANNEL = 'C0139MAV672' # f_upcoming_release

      attr_reader :package_version, :pipeline_url

      def initialize(package_version, pipeline_url)
        @package_version = package_version
        @pipeline_url = pipeline_url
      end

      def to_slack_block
        mention = "<!subteam^#{ReleaseManagers::SlackWrapperClient::RELEASE_MANAGERS_USER_GROUP_ID}>"

        text = StringIO.new
        text.puts(":timer_clock: #{mention} baking time completed")
        text.puts
        text.puts("Package version: `#{package_version}`")
        text.puts("Pipeline: <#{pipeline_url}|deployer>") if pipeline_url

        {
          type: 'section',
          text: ::ReleaseTools::Slack::Webhook.mrkdwn(text.string)
        }
      end
    end
  end
end
