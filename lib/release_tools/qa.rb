# frozen_string_literal: true

module ReleaseTools
  module Qa
    SPECIAL_TEAM_LABELS = [
      'Community contribution'
    ].freeze

    PROJECTS =
      if Feature.enabled?(:qa_multi_project)
        [
          ReleaseTools::Project::GitlabEe,
          ReleaseTools::Project::Gitaly
        ].freeze
      else
        [
          ReleaseTools::Project::GitlabEe
        ].freeze
      end

    def self.group_labels
      @group_labels ||= SPECIAL_TEAM_LABELS + GitlabClient.group_labels('gitlab-org', search: 'group::', per_page: 100)
        .auto_paginate
        .select { |l| l.name.start_with?('group::') }
        .collect(&:name)
    end
  end
end
