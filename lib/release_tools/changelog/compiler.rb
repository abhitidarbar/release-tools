# frozen_string_literal: true

module ReleaseTools
  module Changelog
    # Compiling of Markdown changelogs using the GitLab API.
    class Compiler
      include ::SemanticLogger::Loggable

      # The default source directory to use for finding changelog entries.
      DEFAULT_SOURCE = 'changelogs/unreleased'

      # The name of the default target changelog file.
      DEFAULT_CHANGELOG_FILE = 'CHANGELOG.md'

      # The `project` argument is a project _path_, such as
      # `"gitlab-org/gitlab"`. This argument _should not_ be a
      # `ReleaseTools::Project` class, as this Compiler class should not have
      # any knowledge of regular releases, security releases, etc; it's up to a
      # caller to decide what exact project should be used.
      #
      # The `client` argument is a GitLab API client to use, such as
      # `ReleaseTools::GitlabClient`.
      #
      # The `source` argument specifies the directory to use for finding
      # unreleased changelog entries.
      #
      # The `changelog_file` argument specifies the name of the changelog file
      # to compile entries into.
      def initialize(
        project,
        client: GitlabClient,
        source: DEFAULT_SOURCE,
        changelog_file: DEFAULT_CHANGELOG_FILE
      )
        @project = project
        @client = client
        @source = source
        @changelog_file = changelog_file
      end

      # Compiles the changelog for the given version (a `ReleaseTools::Version`
      # instance).
      #
      # The `branch` argument specifies the branch name to compile the changelog
      # for the release on.
      #
      # The `master` argument specifies if the changelog on the master branch
      # should also be updated.
      def compile(version, branch: version.stable_branch, master: true)
        paths = unreleased_changelog_entry_paths(branch)
        entries = unreleased_changelog_entries(paths, branch)

        compile_entries(version, entries, paths, branch)

        return unless master

        # On "master" some changelog entry files may not exist. Since the
        # content of the changelog is based on the stable branch, we can safely
        # ignore these missing changelog entry files when removing them.
        master_paths = paths & unreleased_changelog_entry_paths('master')

        compile_entries(version, entries, master_paths, 'master')
      end

      # Compiles the given changelog entries on a branch.
      #
      # The `version` argument is a `ReleaseTools::Version` for which to compile
      # the entries.
      #
      # The `entries` argument is an `Array` of `ReleaseTools::Changelog::Entry`
      # instances.
      #
      # The `delete` argument is an `Array` of file paths (as `String`
      # instances) to remove when committing the changes.
      #
      # The `branch` argument is the name of the branch (as a `String`) to
      # compile the changelog on.
      def compile_entries(version, entries, delete, branch)
        markdown_generator = MarkdownGenerator.new(version, entries)

        header = markdown_generator.header
        markdown = markdown_generator.to_s

        # When the changelog is modified concurrently we will retry a few times,
        # making sure to take into account any changes made by another process.
        Retriable.retriable(on: Gitlab::Error::ResponseError) do
          old_changelog = @client.get_file(
            @project,
            @changelog_file,
            branch
          )

          # We use last_commit_id since commit_id returns the latest commit on
          # the whole branch, not the latest commit that changed the changelog
          # file.
          last_commit = old_changelog.last_commit_id
          old_content = Base64
            .decode64(old_changelog.content)
            .force_encoding(Encoding::UTF_8)

          # If somebody else already updated the changelog with our changes, we
          # don't want to add the same Markdown another time.
          return if old_content.include?(markdown.strip)

          # If the compiler is run multiple times (but not concurrently) for the
          # same branch and version, only the first run will add the entries;
          # all other runs would add a "No changes" section.
          #
          # To prevent this from happening, we bail out when trying to add an
          # empty section that already exists.
          return if delete.empty? && old_content.include?(header)

          new_content = Updater.new(old_content, version).insert(markdown)

          commit_changes(version, delete, branch, new_content, last_commit)
        end
      end

      # Commits all changes to the branch.
      #
      # The `version` argument is a `ReleaseTools::Version` the changelog is
      # compiled for.
      #
      # The `delete` argument is an `Array` of file paths (as `String`
      # instances) to remove when committing the changes.
      #
      # The `branch` argument is the name of the branch (as a `String`) to
      # compile the changelog on.
      #
      # The `new_changelog` argument is the content of the compiled changelog
      # file.
      #
      # The `last_commit` argument is the SHA of the last commit made to the
      # changelog file. This is used to prevent overwriting changes when the
      # changelog is updated concurrently.
      def commit_changes(version, delete, branch, new_changelog, last_commit)
        actions = delete.map { |path| { action: 'delete', file_path: path } }

        actions << {
          action: 'update',
          file_path: @changelog_file,
          last_commit_id: last_commit,
          content: new_changelog
        }

        commit = @client.create_commit(
          @project,
          branch,
          "Update #{@changelog_file} for #{version}\n\n[ci skip]",
          actions
        )

        logger.info(
          "Updated changelog file #{@changelog_file}",
          project: @project,
          version: version,
          branch: branch,
          commit: commit.id
        )
      end

      def unreleased_changelog_entry_paths(branch)
        entries = []

        @client
          .tree(@project, ref: branch, path: @source, per_page: 100)
          .auto_paginate do |entry|
            entries << entry.path if entry.name.end_with?('.yml')
          end

        entries
      end

      def unreleased_changelog_entries(paths, branch)
        paths.map do |path|
          content = Retriable.retriable(on: Gitlab::Error::ResponseError) do
            @client.file_contents(@project, path, branch)
          end

          Entry.new(path, content)
        end
      end
    end
  end
end
