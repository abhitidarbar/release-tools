# frozen_string_literal: true

module ReleaseTools
  class UpdateGitalyMergeRequest < MergeRequest
    def title
      "Update Gitaly version"
    end

    def labels
      ['team::Delivery', 'dependency update', 'group::gitaly', 'feature::maintenance']
    end

    def target_branch
      'master'
    end

    protected

    def template_path
      File.expand_path('../../templates/update_gitaly_merge_request.md.erb', __dir__)
    end
  end
end
