# frozen_string_literal: true

desc 'Execute all metrics tasks'
task :metrics do
  tasks = %w[
  ]

  errors = []

  Parallel.each(tasks, in_threads: Etc.nprocessors) do |task|
    Rake::Task["metrics:#{task}"].invoke
  rescue StandardError => ex
    ReleaseTools.logger.warn("Error updating metric", { task: task }, ex)

    errors << ex
  end

  abort if errors.any?
end

namespace :metrics do
end
