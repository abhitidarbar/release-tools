# frozen_string_literal: true

namespace :feature do
  # Undocumented; executed by CI to show feature flag status
  task :list do
    puts ">>> Feature flag status:"

    flags = ::Unleash.toggles&.sort_by { |f| f['name'] } || []
    flags.each do |toggle|
      status = toggle['enabled'] ? '✔' : '𐄂'

      puts "    #{status} #{toggle['name']}"
    end
  end
end

if ENV['CI']
  Rake.application.tasks.each do |task|
    task.enhance %w[feature:list] unless task.name == 'feature:list'
  end
end
